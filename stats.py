#!/usr/bin/env python3

import sys
import psycopg2


def main():
    """ Main """

    # Set up connection to database
    db = setUp()

    # Create the database cursor
    cur = db.cursor()

    # Loop to run queries
    queryLoop(cur)

    # Close the database
    db.close()


def setUp():
    """ Function connects to the database """

    # Validate arguments
    argc = len(sys.argv)
    if argc == 1:
        print("Connecting to default database: asamuels\n")
        connTo = "asamuels"
    elif argc == 2:
        connTo = sys.argv[1]
    else:
        sys.stderr.write("Usage: {} <database>".format(sys.argv[0]))
        exit()

    # Connect to database
    try:
        db = psycopg2.connect(database=connTo)
    except Exception as e:
        sys.stderr.write("Error! {}".format(e))
        exit()

    # Return database
    return(db)


def queryLoop(cur):
    """ Function allows choice of query """

    # Loop until '0' is entered
    while True:
        # Display menu of queries to be ran
        printChoices()

        # Get choice
        choice = input("Enter your choice: ")
        print()

        # Run selected query
        if choice == '1': choice1(cur)
        elif choice == '2': choice2(cur)
        elif choice == '3': choice3(cur)
        elif choice == '4': choice4(cur)
        elif choice == '5': choice5(cur)
        elif choice == '6': choice6(cur)
        elif choice == '0': break
        else:
            print("Invalid choice.")


def choice1(cur):
    """ Function queries the database for the longest fight time """

    # Retrieve biggest finish time
    cur.execute("SELECT MAX(finish) FROM fight")
    longestTime = cur.fetchone()

    # Retrieve corresponding fight table entry
    cur.execute("SELECT * FROM fight WHERE finish=%s", (longestTime,))
    longestFight = cur.fetchone()

    # Retrieve names of fighters involved in longest fight
    cur.execute("SELECT name FROM combatant WHERE id=%s", (longestFight[0],))
    fighter1 = cur.fetchone()
    cur.execute("SELECT name FROM combatant WHERE id=%s", (longestFight[1],))
    fighter2 = cur.fetchone()

    # Calculate duration of fight to be used in seconds
    duration = longestFight[4] - longestFight[3]

    # Display result
    print("{} and {} fought for the longest time! ({}s)".format(fighter1[0], fighter2[0], int(duration.total_seconds())))


def choice2(cur):
    """ Function queries the database for the shortest fight time """

    # Retrieve smallest finish time
    cur.execute("SELECT MIN(finish) FROM fight")
    shortestTime = cur.fetchone()

    # Retrieve corresponding fight table entry
    cur.execute("SELECT * FROM fight WHERE finish=%s", (shortestTime,))
    shortestFight = cur.fetchone()

    # Retrieve names of fighters involved in shortest fight
    cur.execute("SELECT name FROM combatant WHERE id=%s", (shortestFight[0],))
    fighter1 = cur.fetchone()
    cur.execute("SELECT name FROM combatant WHERE id=%s", (shortestFight[1],))
    fighter2 = cur.fetchone()

    # Calculate duration of fight to be used in seconds
    duration = shortestFight[4] - shortestFight[3]

    # Display result
    print("{} VS {} fought for the shortest time! ({}s)".format(fighter1[0], fighter2[0], int(duration.total_seconds())))


def choice3(cur):
    """ Function queries database for the most wins """

    # Initialize variables
    totalWins = 0
    totalWinsId = 0

    # Retrieve total number of fighters
    cur.execute("SELECT COUNT(*) FROM combatant")
    totalCombatants = cur.fetchone()[0]

    # Loop to iterate over fighters and get their number of wins
    for i in range(1, totalCombatants):
        numWins = 0
        # Retrieve all fight table entries where the fighter won
        cur.execute("SELECT * FROM fight WHERE combatant_one=%s AND winner='One'", (i,))
        statsTemp = cur.fetchall()
        if len(statsTemp) != 0:
            stats = statsTemp
            numWins += len(stats)

        # Check if number of wins is the greatest
        if numWins > totalWins:
            totalWins = numWins
            totalWinsId = stats[0][0]

    # Retrieve name of fighter is greatest amount of wins
    cur.execute("SELECT name FROM combatant WHERE id=%s", (totalWinsId,))
    totalWinsName = cur.fetchone()[0]

    # Display result
    print("{} has the most wins! ({})".format(totalWinsName, totalWins))


def choice4(cur):
    """ Function queries database for the most losses """

    # Initialize variables
    totalLosses = 0
    totalLossesId = 0

    # Retrieve total number of fighters
    cur.execute("SELECT COUNT(*) FROM combatant")
    totalCombatants = cur.fetchone()[0]

    # Loop to iterate over fighters and get their number of losses
    for i in range(1, totalCombatants):
        numLosses = 0
        # Retrieve all fight table entries where the fighter lost
        cur.execute("SELECT * FROM fight WHERE combatant_two=%s AND winner='One'", (i,))
        statsTemp = cur.fetchall()
        if len(statsTemp) != 0:
            stats = statsTemp
            numLosses += len(stats)
        
        # Check if number of losses is the greatest
        if numLosses > totalLosses:
            totalLosses = numLosses
            totalLossesId = stats[0][0]

    # Retrieve name of fighter with greatest amount of losses
    cur.execute("SELECT name FROM combatant WHERE id=%s", (totalLossesId,))
    totalLossesName = cur.fetchone()[0]

    # Display result
    print("{} has the most losses! ({})".format(totalLossesName, totalLosses))


def choice5(cur):
    """ Function queries database for the most attacks"""

    # Initialize variables
    mostAttacks = 0
    mostAttacksId = 0

    # Retrieve distinct fighters
    cur.execute("SELECT DISTINCT species_id FROM combatant")
    distinctCombatants = cur.fetchall()

    # Loop to iterate over fighters and get their number of attacks
    for i, in distinctCombatants:
        numAttacks = 0
        # Retrieve all attacks
        cur.execute("SELECT * FROM species_attack WHERE species_id = %s", (i,))
        attackStatsTemp = cur.fetchall()
        if len(attackStatsTemp) != 0:
             attackStats = attackStatsTemp
             numAttacks += len(attackStats)

        # Check if number of attacks is the greatest
        if numAttacks > mostAttacks:
            mostAttacks = numAttacks
            mostAttacksId = attackStats[0][0]

    # Retrieve name of fighter with greatest amount of attacks
    cur.execute("SELECT name FROM combatant WHERE species_id=%s", (mostAttacksId,))
    mostAttacksName = cur.fetchone()

    # Display result
    print("{} has the most attacks! ({})".format(mostAttacksName[0], mostAttacks))


def choice6(cur):
    """ Function queries database and displays a table of w/l/d of each species"""

    # Initialize lists of stats
    wins = [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    losses = [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    draws = [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    # Retrieve all fights from fight table
    cur.execute("SELECT * FROM fight")
    allFights = cur.fetchall()

    # Loop to parse each fight
    for fight in allFights:
        if fight[2] == 'One':
            # Update list of stats
            cur.execute("SELECT species_id FROM combatant WHERE id=%s", (fight[0],))
            spec = cur.fetchone()[0]
            wins[spec] += 1
            cur.execute("SELECT species_id FROM combatant WHERE id=%s", (fight[1],))
            spec = cur.fetchone()[0]
            losses[spec] += 1
        elif fight[2] == 'Two':
            # Update list of stats
            cur.execute("SELECT species_id FROM combatant WHERE id=%s", (fight[1],))
            spec = cur.fetchone()[0]
            wins[spec] += 1
            cur.execute("SELECT species_id FROM combatant WHERE id=%s", (fight[0],))
            spec = cur.fetchone()[0]
            losses[spec] += 1
        else:
            # Update list of stats
            cur.execute("SELECT species_id FROM combatant WHERE id=%s", (fight[0],))
            spec = cur.fetchone()[0]
            draws[spec] += 1
            cur.execute("SELECT species_id FROM combatant WHERE id=%s", (fight[1],))
            spec = cur.fetchone()[0]
            draws[spec] += 1

    # Display table header
    print("{0:20} {1:^6} {2:^8} {3:^7}".format("Species Name", "Wins", "Losses", "Draws"))

    for i in range(1, len(wins)):
        # Retrieve name of current speices
        cur.execute("SELECT name FROM species WHERE id=%s", (i,))
        curName = cur.fetchone()[0]

        # Replace 0's with -'s
        if wins[i] == 0:
            wins[i] = '-'
        if losses[i] == 0:
            losses[i] = '-'
        if draws[i] == 0:
            draws[i] = '-'

        # Display stats for current species
        print("{0:20} {1:^6} {2:^8} {3:^7}".format(curName, wins[i], losses[i], draws[i]))


def printChoices():
    """ Function prints out menu of possible queries """

    # Menu of choices
    print("=====================")
    print("1. Longest Fight")
    print("2. Shortest Fight")
    print("3. Most wins")
    print("4. Most losses")
    print("5. Most possible attacks")
    print("6. Wins/Losses/Draws by Species")
    print("\n0. Exit")
    print("=====================")


if __name__ == "__main__":
    main()


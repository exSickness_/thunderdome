#!/usr/bin/python3

import psycopg2
import sys


def main():
    """ Main """

    # Check for correct arguments
    argc = len(sys.argv)
    if argc == 1:
        # Connect to default: asamuels
        print("Connecting to default database: asamuels")
        connTo = "asamuels"
    elif argc == 2:
        # Connect to argument
        connTo = sys.argv[1]
    else:
        # Invalid number of arguments
        sys.stderr.write("Usage: {} <database>".format(sys.argv[0]))
        exit()

    try:
        # Connect to database
        db = psycopg2.connect(database='asamuels')
        cur = db.cursor()

        # Reset fight and combatant tables
        cur.execute("DELETE FROM fight *")
        cur.execute("DELETE FROM combatant *")

        # Reset combatant ids
        cur.execute("ALTER SEQUENCE combatant_id_seq RESTART;")
        cur.execute("UPDATE combatant SET id = DEFAULT;")

        # Update database
        db.commit()
        db.close()

    except Exception as e:
        sys.stderr.write("Error!", e)
        exit()

if __name__ == "__main__":
    main()


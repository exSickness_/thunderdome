#!/usr/bin/env python3

import psycopg2
import random as rdm
import datetime as dt
import sys


def main():
    """ Main """

    db = setUp()  # Set up connection to database

    cur = db.cursor()  # Create the database cursor

    checkTables(cur)  # Ensure new tournament

    populateCombatants(db, cur)  # Create combatants to fight

    fightLoop(cur)  # Fight afore mentioned combatants

    db.commit()  # Update database with results

    db.close()  # Close connection to database


def setUp():
    """ Function connects to the database """

    # Validate arguments
    argc = len(sys.argv)

    if argc == 1:
        print("Connecting to default database: asamuels")
        connTo = "asamuels"
    elif argc == 2:
        connTo = sys.argv[1]
    else:
        printf("Usage: {} <database>".format(sys.argv[0]))
        exit()

    # Connect to database
    try:
        db = psycopg2.connect(database=connTo)
    except Exception as e:
        sys.stderr.write("Error! {}".format(e))
        exit()

    # Return database
    return(db)


def checkTables(cur):
    """ Function checks to make sure a tournament has not already taken place """

    # Test if the fight table has data
    cur.execute("SELECT * FROM fight")
    test = cur.fetchone()
    if test is not None:
        sys.stderr.write("The tournament has already taken place.\n")
        exit()


def populateCombatants(db, cur):
    """ Function populates the combatant table in random order"""

    # Combatants to fight
    combatantList = [
                        ("Sick",        rdm.randint(1, 14), 0, 0, 0),
                        ("Vile",        rdm.randint(1, 14), 0, 0, 0),
                        ("Virus",       rdm.randint(1, 14), 0, 0, 0),
                        ("Wicked",      rdm.randint(1, 14), 0, 0, 0),
                        ("Ill",         rdm.randint(1, 14), 0, 0, 0),
                        ("Infect",      rdm.randint(1, 14), 0, 0, 0),
                        ("Rotten",      rdm.randint(1, 14), 0, 0, 0),
                        ("Infirm",      rdm.randint(1, 14), 0, 0, 0),
                        ("Disease",     rdm.randint(1, 14), 0, 0, 0),
                        ("Corrupt",     rdm.randint(1, 14), 0, 0, 0),
                        ("Vicious",     rdm.randint(1, 14), 0, 0, 0),
                        ("Debilitate",  rdm.randint(1, 14), 0, 0, 0),
                        ("Noxious",     rdm.randint(1, 14), 0, 0, 0)
                ]

    # Shuffle combatants
    rdm.shuffle(combatantList)

    # Insert shuffled combatants
    cur.executemany("INSERT INTO combatant (name, species_id, plus_atk, plus_dfn, plus_hp) VALUES (%s,%s,%s,%s,%s)", combatantList)

    # Update database
    db.commit()


def fightLoop(cur):
    """ Function fights every combatant with the other combatants """

    # Get number of combatants to iterate over
    cur.execute("SELECT COUNT(*) FROM combatant")
    totalCombatants = cur.fetchone()[0]

    # Fight each combatant with every other
    for f1 in range(1, totalCombatants - 1):
        for f2 in range(f1+1, totalCombatants):
            currentFight(cur, f1, f2)


def currentFight(cur, f1, f2):
    """ Function fights two combatants and stores result in fight table """

    # Retrieve name for fighter 1
    cur.execute("SELECT name FROM combatant WHERE id=%s", (f1,))
    combatantName1 = cur.fetchone()[0]

    # Retrieve stats for fighter 1
    cur.execute("SELECT * FROM species WHERE id=%s", (f1,))
    id1, name1, type1, baseA1, baseD1, baseH1 = cur.fetchone()

    # Retrieve possible attacks for fighter 1
    cur.execute("SELECT attack_id FROM species_attack WHERE species_id = %s", (id1,))
    attackIds1 = cur.fetchall()
    attackList1 = []
    for atk, in attackIds1:
        cur.execute("SELECT * FROM attack WHERE id = %s", (atk,))
        attackList1.append(cur.fetchone())

    # Retrieve name for fighter 2
    cur.execute("SELECT name FROM combatant WHERE id=%s", (f2,))
    combatantName2 = cur.fetchone()[0]

    # Retrieve stats for fighter 2
    cur.execute("SELECT * FROM species WHERE id=%s", (f2,))
    (id2, name2, type2, baseA2, baseD2, baseH2) = cur.fetchone()

    # Retrieve possible attacks for fighter 2
    cur.execute("SELECT attack_id FROM species_attack WHERE species_id = %s", (id2,))
    attackIds2 = cur.fetchall()
    attackList2 = []
    for atk, in attackIds2:
        cur.execute("SELECT * FROM attack WHERE id = %s", (atk,))
        attackList2.append(cur.fetchone())

    # Display fighter 1 vs fighter 2
    print("{} vs {}\n".format(combatantName1, combatantName2))

    # Initialize variables for fight sequence
    step = 1
    winner = 0
    attack1 = rdm.choice(attackList1)
    attack2 = rdm.choice(attackList2)
    atkTime1 = step + attack1[5].total_seconds()
    atkTime2 = step + attack2[5].total_seconds()

    # Fight sequence loop
    while True:
        if atkTime1 == step and atkTime2 == step:
            # Possible draw condition

            # Apply damage and set up next attack for fighter 1
            damage1 = getDamage(baseD2, baseA1, attack1[3], attack1[4], type1, type2, attack1[2])
            print("{} used {} and did {} damage!".format(combatantName1, attack1[1], damage1))
            baseH2 -= damage1
            attack1 = rdm.choice(attackList1)
            atkTime1 = step + attack1[5].total_seconds()

            # Apply damage and set up next attack for fighter 2
            damage2 = getDamage(baseD1, baseA2, attack2[3], attack2[4], type2, type1, attack2[2])
            print("  {} used {} and did {} damage!".format(combatantName2, attack2[1], damage2))
            baseH1 -= damage2
            attack2 = rdm.choice(attackList2)
            atkTime2 = step + attack2[5].total_seconds()

            # Check for downed fighters
            if baseH1 < 1 and baseH2 < 1:
                # Draw
                winner = 3
                break
            elif baseH1 < 1:
                # Fighter 2 wins
                winner = 2
            elif baseH2 < 1:
                # Fighter 1 wins
                winner = 1

        elif atkTime1 == step:
            # Fighter 1 attack is ready

            # Apply damage and set up next attack
            damage1 = getDamage(baseD2, baseA1, attack1[3], attack1[4], type1, type2, attack1[2])
            print("{} used {} and did {} damage!".format(combatantName1, attack1[1], damage1))
            baseH2 -= damage1
            attack1 = rdm.choice(attackList1)
            atkTime1 = step + attack1[5].total_seconds()

            # Check if downed fighter 2
            if baseH2 < 1:
                # Fighter 1 wins
                winner = 1
                break

        elif atkTime2 == step:
            # Fighter 2 attack is ready

            # Apply damage and set up next attack
            damage2 = getDamage(baseD1, baseA2, attack2[3], attack2[4], type2, type1, attack2[2])
            print("  {} used {} and did {} damage!".format(combatantName2, attack2[1], damage2))
            baseH1 -= damage2
            attack2 = rdm.choice(attackList2)
            atkTime2 = step + attack2[5].total_seconds()

            # Check if downed fighter 1
            if baseH1 < 1:
                # Fighter 2 wins
                winner = 2
                break
        # Check for stalemate condition
        if step > 500:
            winner = 3
            break

        # Increment timer step
        step += 1

    print()

    # Create a string formatted for type TIMESTAMP
    finishStr = getTimeStamp(step)

    # Input fight results into fight table
    if winner == 3:
        # Draw
        print("There was a draw!")
        cur.execute("INSERT INTO fight VALUES (%s, %s, 'Tie', TIMESTAMP '1000-01-01 00:00:00',TIMESTAMP %s)", (f1, f2, finishStr))

    elif winner == 1:
        # Fighter 1 won
        print("{} has defeated {}!".format(combatantName1, combatantName2))
        cur.execute("INSERT INTO fight VALUES (%s, %s, 'One', TIMESTAMP '1000-01-01 00:00:00',TIMESTAMP %s)", (f1, f2, finishStr))

    else:
        # Fighter 2 won
        print("{} has defeated {}!".format(combatantName2, combatantName1))
        cur.execute("INSERT INTO fight VALUES (%s, %s, 'Two', TIMESTAMP '1000-01-01 00:00:00', TIMESTAMP %s)", (f1, f2, finishStr))

    print("======================\n")


def getDamage(baseD, baseA, minDmg, maxDmg, type1, type2, a_type):
    """ Function runs damage formula """

    # Get modifiers used into attack formula
    typeMod = getMod(type1, type2)
    atkMod = getMod(a_type, type2)

    # Attack formula
    damage = typeMod * rdm.randint(0, abs(baseD - baseA)) + atkMod * rdm.randint(minDmg, maxDmg)

    # Return damage
    return(damage)


def getTimeStamp(step):
    """ Function returns correct format for TIMESTAMP entry """

    # Initial part of string
    finishStr = "'1000-01-01 00:"

    # Append on minutes
    temp = int(step / 60)
    if temp < 10:
        finishStr += "0"
    finishStr += str(temp)
    finishStr += ":"

    # Append on seconds
    temp = step % 60
    if temp < 10:
        finishStr += "0"
    finishStr += str(temp)
    finishStr += "'"

    # Return correctly formatted string
    return(finishStr)


def getMod(type1, type2):
    """ Function returns the damage modifier based upon types """

    # Default modifier
    mod = 1

    # Begin checks for different modifer based upon type
    if type1 == 'Physical':
        if type2 == 'Biological':
            mod = 2
        elif type2 == 'Chemical':
            mod = .5

    elif type1 == 'Biological':
        if type2 == 'Radioactive':
            mod = 2
        elif type2 == 'Technological':
            mod = 2

    elif type1 == 'Radioactive':
        if type2 == 'Physical':
            mod = 2
        elif type2 == 'Chemical':
            mod = .5
        elif type2 == 'Technological':
            mod = .5
        elif type2 == 'Mystical':
            mod = 2

    elif type1 == 'Chemical':
        if type2 == 'Biological':
            mod = 2
        elif type2 == 'Radioactive':
            mod = .5
        elif type2 == 'Chemical':
            mod = .5
        elif type2 == 'Technological':
            mod = .5

    elif type1 == 'Technological':
        if type2 == 'Radioactive':
            mod = .5
        elif type2 == 'Chemical':
            mod = 2

    elif type1 == 'Mystical':
        if type2 == 'Physical':
            mod = .5
        elif type2 == 'Biological':
            mod = 2
        elif type2 == 'Radioactive':
            mod = 2
        elif type2 == 'Chemical':
            mod = .5
        elif type2 == 'Technological':
            mod = .5
        elif type2 == 'Mystical':
            mod = .5

    elif type1 == 'Mineral':
        if type2 == 'Mineral':
            mod = 0

    return(mod)


if __name__ == "__main__":
    main()

